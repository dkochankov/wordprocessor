package org.example;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.*;
import java.util.stream.Collectors;

public class WordProcessor {
    private static final int MAX_WORD_SIZE = 9;
    private static final int ITERATIONS = 6;
    private static final StringBuffer charA = new StringBuffer("A");
    private static final StringBuffer charI = new StringBuffer("I");

    public static void main(String[] args) throws IOException {
        List<String> words = loadWords();
        List<String> valid = getValidWords(words);
        System.out.println("VALID WORDS: " + valid);
    }

    public static List<String> getValidWords(List<String> words) {
        Set<String> allWords =new HashSet<>(words);
        List<String> result = new ArrayList<>();

        long startTime = System.nanoTime();
        System.out.println("start time: " + startTime);

        /*
        for (String w : words) {
            if (w.length() == MAX_WORD_SIZE && validateWord(w, allWords)) {
                result.add(w);
            }
        }
         */

        words.parallelStream().filter(w -> w.length() == MAX_WORD_SIZE && validateWord(w, allWords)).forEach(result::add);
        long endTime = System.nanoTime();
        long processingTime = endTime - startTime;
        System.out.println("end time: " + System.nanoTime());
        System.out.println("processing time: " + processingTime);
        System.out.println("result size: " + result.size());
        return result;
    }

    private static boolean validateWord(String word, Set<String> words) {
        Map<Integer, Set<String>> sets = initializeSets();
        transformWord(word, words, sets);
        if (sets.get(0).isEmpty()) { return false; }
        System.out.println("INITIAL SET: " + sets.get(0));

        for (int i=0; i<=ITERATIONS-1; i++) {
            if (!validateIterationStep(words, i, sets)) {
                return false;
            }
        }
        return validateChars(sets.get(ITERATIONS));
    }

    private static Map<Integer, Set<String>> initializeSets() {
        Map<Integer, Set<String>> sets = new HashMap<>();

        for(int i = 0; i<=ITERATIONS; i++) {
            sets.put(i, new HashSet<String>());
        }

        return sets;
    }

    private static boolean validateIterationStep(Set<String> words, int iteration, Map<Integer, Set<String>> processMap) {
        int newSetIndex = iteration+1;
        for (String s : processMap.get(iteration)) {
            StringBuffer transformedWord = new StringBuffer();
            transformedWord.append(s);
            for (int n = 0; n < MAX_WORD_SIZE - 1 - iteration; n++) {
                transformedWord.deleteCharAt(n);
                if (!words.contains(transformedWord.toString())) {
                    transformedWord.replace(0, s.length(), s);
                    continue;
                }
                processMap.get(newSetIndex).add(transformedWord.toString());
                transformedWord.replace(0, s.length(), s);
            }
        }
        if (processMap.get(newSetIndex).isEmpty()) {
            return false;
        }

        System.out.println("Iteration list " + iteration + ": " + processMap.get(newSetIndex));
        return true;
    }

    private static boolean validateChars(Set<String> input) {
        boolean containsChar = false;
        for(String s : input) {
            StringBuffer transformedWord = new StringBuffer();
            transformedWord.append(s);
            for (int n = 0; n < 2; n++) {
                transformedWord.deleteCharAt(n);
                System.out.println("char validation: " + transformedWord);
                if (transformedWord.compareTo(charA) == 0 || transformedWord.compareTo(charI) == 0) {
                    containsChar = true;
                }
                transformedWord.replace(0, s.length(), s);
            }
        }
        return containsChar;
    }

    private static void transformWord(String word, Set<String> words, Map<Integer, Set<String>> sets) {
        StringBuilder transformedWord = new StringBuilder();
        transformedWord.append(word);
        for (int n = 0; n < MAX_WORD_SIZE; n++) {
            transformedWord.deleteCharAt(n);
            if (!words.contains(transformedWord.toString())) {
                transformedWord.replace(0, word.length(), word);
                continue;
            }
            sets.get(0).add(transformedWord.toString());
            transformedWord.replace(0, word.length(), word);
        }
    }

    private static List<String> loadWords() throws IOException {
        URL wordList = new URL("https://raw.githubusercontent.com/nikiiv/JavaCodingTestOne/master/scrabble-words.txt");
        try(BufferedReader br = new BufferedReader(new InputStreamReader(wordList.openConnection().getInputStream()))) {
            return br.lines().skip(2).collect(Collectors.toList());
        }
    }

}
